<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class BaseController extends Controller
{
    public function __construct($id, $module, $config = [])
    {
        Yii::$app->name = 'Bintime Test Task';
        parent::__construct($id, $module, $config);
    }
}
