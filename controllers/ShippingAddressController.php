<?php

namespace app\controllers;

use app\repositories\ShippingAddressRepository;
use app\models\ShippingAddress;
use app\models\ShippingAddressSearch;
use app\repositories\UserRepository;
use Exception;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Throwable;
use Yii;

/**
 * ShippingAddressController implements the CRUD actions for ShippingAddress model.
 */
class ShippingAddressController extends BaseController
{
    /* @var ShippingAddress */
    private $shippingAddress;
    /* @var ShippingAddressRepository */
    private $shippingAddressRepository;
    /* @var ShippingAddressSearch */
    private $shippingAddressSearch;
    /* @var UserRepository */
    private $userRepository;

    /**
     * ShippingAddressController constructor.
     * {@inheritdoc}
     * @param ShippingAddress $shippingAddress
     * @param ShippingAddressRepository $shippingAddressRepository
     * @param ShippingAddressSearch $shippingAddressSearch
     */
    public function __construct(
        $id,
        $module,
        ShippingAddress $shippingAddress,
        ShippingAddressRepository $shippingAddressRepository,
        ShippingAddressSearch $shippingAddressSearch,
        UserRepository $userRepository,
        $config = []
    )
    {
        $this->shippingAddress = $shippingAddress;
        $this->shippingAddressRepository = $shippingAddressRepository;
        $this->shippingAddressSearch = $shippingAddressSearch;
        $this->userRepository = $userRepository;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new ShippingAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionCreate()
    {
        if ($this->shippingAddress->load(Yii::$app->request->post()) && $this->shippingAddress->save()) {
            Yii::$app->session->setFlash('success', 'Shipping address successfully created.');
            return $this->redirect(['/user/view', 'id' => Yii::$app->request->get('id')]);
        }

        return $this->render('create', [
            'model' => $this->shippingAddress,
            'user' => $this->userRepository->findUserById(Yii::$app->request->get('id')),
        ]);
    }

    /**
     * Updates an existing ShippingAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->shippingAddressRepository->findShippingAddressById($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Shipping address successfully updated.');
            return $this->redirect(['/user/view', 'id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShippingAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->shippingAddressRepository->findShippingAddressById($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Shipping address successfully deleted.');

        return $this->redirect(['/user/view', 'id' => $model->user_id]);
    }
}
