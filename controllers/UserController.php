<?php

namespace app\controllers;

use app\models\NewUserForm;
use app\models\ShippingAddress;
use app\models\ShippingAddressSearch;
use app\repositories\UserRepository;
use app\models\User;
use app\models\UserSearch;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Exception;
use Throwable;
use Yii;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{
    /* @var User */
    private $user;
    /* @var ShippingAddress */
    private $shippingAddress;
    /* @var NewUserForm */
    private $newUserForm;
    /* @var UserSearch */
    private $userSearch;
    /* @var UserRepository */
    private $userRepository;
    /* @var ShippingAddressSearch */
    private $shippingAddressSearch;

    /**
     * UserController constructor.
     * {@inheritdoc}
     * @param User $user
     * @param UserRepository $userRepository
     * @param UserSearch $userSearch
     */
    public function __construct(
        $id,
        $module,
        User $user,
        ShippingAddress $shippingAddress,
        NewUserForm $newUserForm,
        UserRepository $userRepository,
        UserSearch $userSearch,
        ShippingAddressSearch $shippingAddressSearch,
        $config = []
    )
    {
        $this->user = $user;
        $this->shippingAddress = $shippingAddress;
        $this->newUserForm = $newUserForm;
        $this->userRepository = $userRepository;
        $this->userSearch = $userSearch;
        $this->shippingAddressSearch = $shippingAddressSearch;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'searchModel' => $this->userSearch,
            'dataProvider' => $this->userSearch->search(Yii::$app->request->queryParams),
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->userRepository->findUserById($id);
        $dataProvider = $this->shippingAddressSearch->search($model->id, Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'genre' => $model->getGenre(),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
    public function actionCreate()
    {
        if ($this->newUserForm->load(Yii::$app->request->post()) && $this->newUserForm->create()) {
            Yii::$app->session->setFlash('success', 'User successfully created.');
            return $this->redirect(['/user/index']);
        }

        return $this->render('create', [
            'model' => $this->newUserForm,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->userRepository->findUserById($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'User successfully updated.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->userRepository->findUserById($id)->delete();
        Yii::$app->session->setFlash('success', 'User successfully deleted.');

        return $this->redirect(['/user/index']);
    }
}
