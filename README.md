# BINTIME Test Task

Documentation of Yii 2 Basic Project Template is at [README.md](https://github.com/yiisoft/yii2-app-basic/blob/master/README.md).

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-basic.svg)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://packagist.org/packages/yiisoft/yii2-app-basic)

Requirements to development
-------------------
- PHP version 7.3
- PostgreSQL
- Nginx
- Composer
- Git
- Linux / Mac OS

Preparing project
-------------------

Clone project to the local machine:
```
SSH:
git clone git@bitbucket.org:pro100rost/bintime-test-task.git

HTTPS:
git clone https://pro100rost@bitbucket.org/pro100rost/bintime-test-task.git
```

The first steps after clone project to the local machine:

- Open a console terminal and install all Composer requirements:
```
composer install
```

- Run the command in terminal one by one to create database:
```
sudo -u postgres psql postgres
# CREATE USER "test" WITH PASSWORD '1234';
# CREATE DATABASE test_db WITH OWNER "test";
# GRANT ALL PRIVILEGES ON DATABASE test_db to "test";
# \quit
sudo systemctl restart postgresql
```

- Adjust the db configuration in file `config/db.php` like a:
```
'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=test_db',
    'username' => 'test',
    'password' => '1234',
    'charset' => 'utf8',
```

- Open a console terminal and apply migrations with command:
 ```
 php yii migrate
 ```

- You can create test data by running following commands:
```
php yii test-data/insert - to add test data
php yii test-data/delete - to delete all data
```

Enjoy :)
