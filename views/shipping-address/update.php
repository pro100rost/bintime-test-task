<?php

use app\models\ShippingAddress;
use app\models\User;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model ShippingAddress */
/* @var $user User */

$user = $model->user;
$this->title = 'Update User\'s Shipping Address';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => $user->username ?? '', 'url' => ['/user/view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shipping-address-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('address-form', [
        'model' => $model,
    ]) ?>

</div>
