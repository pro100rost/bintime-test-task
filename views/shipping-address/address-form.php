<?php

use app\models\ShippingAddress;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model ShippingAddress */
/* @var $form ActiveForm */
?>

<div class="shipping-address-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form
                ->field($model, 'zip_code')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your zip code...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'country')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your country code...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'city')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your city...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'street')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your street...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'house_number')
                ->textInput(['placeholder' => 'Your house number...'])
            ?>

            <?php echo $form
                ->field($model, 'flat_number')
                ->textInput(['placeholder' => 'Your flat/office number...'])
            ?>

            <div class="form-group">
                <?php echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
