<?php

use app\models\ShippingAddress;
use app\models\User;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $model ShippingAddress */
/* @var $user User */

$this->title = 'Create Shipping Address';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['/user/view', 'id' => Yii::$app->request->get('id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-address-create">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('address-form', [
        'model' => $model,
    ]) ?>

</div>
