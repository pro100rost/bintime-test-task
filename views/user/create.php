<?php

use app\models\NewUserForm;
use app\models\User;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model NewUserForm */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-create">

    <div class="row">

        <div class="col-lg-1"></div>

        <?php $form = ActiveForm::begin(['options' => ['tag' => false]]); ?>

        <div class="col-lg-5">

            <h2><?php echo Html::encode('User information') ?></h2>
            <hr>

            <?php echo $form
                ->field($model, 'first_name')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your first name...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'last_name')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your last name...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'genre')
                ->dropDownList(
                    [
                        User::GENRE_MALE => 'Male',
                        User::GENRE_FEMALE => 'Female',
                        User::GENRE_NO_INFORMATION => 'No information',
                    ],
                    ['prompt' => 'Select genre...'])
            ?>

            <?php echo $form
                ->field($model, 'username')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your username...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'email')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your email...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'password')
                ->passwordInput(['placeholder' => 'Your password...'])
            ?>

            <?php echo $form
                ->field($model, 'confirm_password')
                ->passwordInput(['placeholder' => 'Confirm password...'])
            ?>

        </div>
        <div class="col-lg-5">

            <h2><?php echo Html::encode('Shipping address information') ?></h2>
            <hr>

            <?php echo $form
                ->field($model, 'zip_code')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your zip code...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'country')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your country code...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'city')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your city...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'street')
                ->textInput(
                    [
                        'maxlength' => true,
                        'placeholder' => 'Your street...',
                    ])
            ?>

            <?php echo $form
                ->field($model, 'house_number')
                ->textInput(['placeholder' => 'Your house number...']) ?>

            <?php echo $form
                ->field($model, 'flat_number')
                ->textInput(['placeholder' => 'Your flat/office number...']) ?>

        </div>

        <div class="col-lg-1" style="clear: both"></div>

        <div class="col-lg-10">

            <?php echo Html::submitButton('Save', ['class' => 'btn btn-large btn-block btn-success']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
