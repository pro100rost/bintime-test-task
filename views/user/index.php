<?php

use app\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\web\View;

/* @var $this View */
/* @var $searchModel UserSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <div class="row">

        <?php echo $this->render('search-form', ['model' => $searchModel]); ?>

        <?php echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'first_name',
                'last_name',
                'username',
                'created_time:datetime',
                'email:email',
                ['class' => 'yii\grid\ActionColumn'],
            ],
            'pager' => [
                'prevPageLabel' => 'Previous',
                'nextPageLabel' => 'Next',
            ],
            'tableOptions' => ['class' => 'table table-hover'],
            'options' => [
                'class' => 'col-lg-9',
            ],
        ]); ?>

    </div>

</div>
