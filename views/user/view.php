<?php

use app\models\ShippingAddress;
use app\models\ShippingAddressSearch;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this View */
/* @var $model User */
/* @var $searchModel ShippingAddressSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $genre string */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>

<div class="user-view">

    <h1><?php echo Html::encode($model->first_name . ' ' . $model->last_name) ?></h1>

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php echo Html::a('Create Shipping Address', ['/shipping-address/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <dl class="dl-horizontal">

        <?php echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'username',
                'first_name',
                'last_name',
                [
                    'label' => 'Genre',
                    'value' => $genre,
                ],
                'created_time:datetime',
                'email:email',
            ],
            'template' => '<dt{captionOptions}>{label}</dt><dd{contentOptions}>{value}</dd>',
        ]) ?>

    </dl>


    <div class="row">
        <ul class="thumbnails">

            <?php echo ListView::widget([
                'options' => [
                    'class' => 'listing-products',
                    'tag' => false,
                ],
                'dataProvider' => $dataProvider,
                'emptyText' => 'Shipping addresses not found.',
                'itemView' => function ($model, $key, $index, $widget) {
                    /* @var $model ShippingAddress */
                    echo '<li class="col-lg-4" style="list-style-type: none;">';
                    echo '<div class="thumbnail">';
                    echo '<div class="caption">';
                    echo '<h3>Shipping address</h3>';
                    echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'zip_code',
                            'country',
                            'city',
                            'street',
                            'house_number',
                            'flat_number',
                        ],
                        'template' => '<p{captionOptions}><strong>{label}: </strong>{value}</p><p></p>',
                    ]);
                    echo Html::a('Update', [
                            '/shipping-address/update',
                            'id' => $model->id,
                        ], ['class' => 'btn btn-primary']) . ' ';
                    echo Html::a('Delete', [
                        '/shipping-address/delete',
                        'id' => $model->id,
                    ], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                    echo '</div></div></li>';
                },
                'layout' => "{items}\n</div><div class=\"container\">{pager}</div>",
                'pager' => [
                    'prevPageLabel' => 'Previous',
                    'nextPageLabel' => 'Next',
                ],
            ]) ?>

        </ul>
    </div>

</div>
