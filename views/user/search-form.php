<?php

use app\models\User;
use app\models\UserSearch;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model UserSearch */
/* @var $form ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'class' => 'col-lg-3',
        ],
    ]); ?>

    <?php echo $form->field($model, 'searchName') ?>

    <?php echo $form
        ->field($model, 'genre_id')
        ->dropDownList(
            [
                User::GENRE_MALE => 'Male',
                User::GENRE_FEMALE => 'Female',
                User::GENRE_NO_INFORMATION => 'No information',
            ],
            [
                'prompt' => 'Select genre...',
            ]
        ) ?>

    <?php echo $form
        ->field($model, 'createdFrom')
        ->widget(DateTimePicker::class,
            [
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy hh:ii',
                    'autoclose' => true,
                    'weekStart' => 1,
                    'todayBtn' => true,
                ]
            ]
        ) ?>

    <?php echo $form
        ->field($model, 'createdTo')
        ->widget(DateTimePicker::class,
            [
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy hh:ii',
                    'autoclose' => true,
                    'weekStart' => 1,
                    'todayBtn' => true,
                ]
            ]
        ) ?>

    <?php // echo $form->field($model, 'email') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
