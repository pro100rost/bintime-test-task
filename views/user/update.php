<?php

use app\models\User;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model User */

$this->title = 'Update User: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="user-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="row">

        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

            <?php echo $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'genre_id')->dropDownList(
                [
                    User::GENRE_MALE => 'Male',
                    User::GENRE_FEMALE => 'Female',
                    User::GENRE_NO_INFORMATION => 'No information',
                ],
                [
                    'prompt' => 'Select genre...',
                ]) ?>

            <?php echo $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'email')->textInput([
                    'maxlength' => true,
                    'placeholder' => 'Your email...',
                ]
            ) ?>

            <div class="form-group">
                <?php echo Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
