<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 * @property string $searchName
 * @property string $createdFrom
 * @property string $createdTo
 */
class UserSearch extends User
{
    public $searchName;
    public $createdFrom;
    public $createdTo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'password_hash', 'first_name', 'last_name', 'created_time', 'email', 'auth_key'], 'safe'],
            [['searchName', 'createdFrom', 'createdTo'], 'safe'],
            [['genre_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ilike', 'username', $this->searchName])
            ->orFilterWhere(['ilike', 'first_name', $this->searchName])
            ->orFilterWhere(['ilike', 'last_name', $this->searchName])
            ->orFilterWhere(['ilike', 'email', $this->searchName]);

        $query->andFilterWhere([
            'genre_id' => $this->genre_id,
        ]);

        $query->andFilterWhere(['>=', 'created_time', $this->createdFrom])
            ->andFilterWhere(['<=', 'created_time', $this->createdTo]);

        return $dataProvider;
    }
}
