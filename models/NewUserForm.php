<?php

namespace app\models;

use Ramsey\Uuid\Uuid;
use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * @property string $first_name
 * @property string $last_name
 * @property int $genre
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $confirm_password
 *
 * @property string $zip_code
 * @property string $country
 * @property string $city
 * @property string $street
 * @property int $house_number
 * @property int $flat_number
 *
 */
class NewUserForm extends Model
{
    public $first_name;
    public $last_name;
    public $genre;
    public $username;
    public $email;
    public $password;
    public $confirm_password;
    public $zip_code;
    public $country;
    public $city;
    public $street;
    public $house_number;
    public $flat_number;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'genre', 'username', 'email', 'password', 'confirm_password'], 'required'],

            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'match', 'pattern' => '/[A-Z]{1}[a-z]+/', 'message' => 'You can only use latin letters starting with a capital letter.'],

            ['genre', 'default', 'value' => User::GENRE_NO_INFORMATION],
            ['genre', 'in', 'range' => [User::GENRE_MALE, User::GENRE_FEMALE, User::GENRE_NO_INFORMATION]],
            [['genre'], 'integer'],

            ['username', 'string', 'min' => 4],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],

            [['email'], 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email has already been taken.'],

            [['password', 'confirm_password'], 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'confirm_password'],

            [['zip_code', 'country', 'city', 'street', 'house_number'], 'required'],

            [['zip_code'], 'string', 'max' => 10],
            [['zip_code'], 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'You can only use numbers.'],

            [['country'], 'string', 'max' => 2],
            [['country'], 'match', 'pattern' => '/^[A-Z]{2}$/', 'message' => 'You can only use two uppercase latin letters.'],

            [['city', 'street'], 'string', 'max' => 255],

            [['house_number', 'flat_number'], 'integer'],
            ['flat_number', 'default', 'value' => null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'genre_id' => 'Genre',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'zip_code' => 'Zip Code',
            'country' => 'Country',
            'city' => 'City',
            'street' => 'Street',
            'house_number' => 'House Number',
            'flat_number' => 'Flat Number',
        ];
    }

    /**
     * @return bool|null
     * @throws Exception
     * @throws \Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $userId = Uuid::uuid4()->toString();
        $user->id = $userId;
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->genre_id = $this->genre;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->created_time = date('d-m-Y H:i:s');
        if (!$user->save()) {
            Yii::$app->session->addFlash('error', 'Cannot create user.');
        }

        $shippingAddress = new ShippingAddress();
        $shippingAddress->id = Uuid::uuid4()->toString();
        $shippingAddress->zip_code = $this->zip_code;
        $shippingAddress->country = $this->country;
        $shippingAddress->city = $this->city;
        $shippingAddress->street = $this->street;
        $shippingAddress->house_number = $this->house_number;
        $shippingAddress->flat_number = $this->flat_number;
        $shippingAddress->user_id = $userId;

        return $shippingAddress->save();
    }
}
