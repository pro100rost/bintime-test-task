<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ShippingAddressSearch represents the model behind the search form of `app\models\ShippingAddress`.
 */
class ShippingAddressSearch extends ShippingAddress
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'zip_code', 'country', 'city', 'street', 'user_id'], 'safe'],
            [['house_number', 'flat_number'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param string $id
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(string $id, array $params)
    {
        $query = ShippingAddress::find()->where(['user_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'house_number' => $this->house_number,
            'flat_number' => $this->flat_number,
        ]);

        $query->andFilterWhere(['ilike', 'id', $this->id])
            ->andFilterWhere(['ilike', 'zip_code', $this->zip_code])
            ->andFilterWhere(['ilike', 'country', $this->country])
            ->andFilterWhere(['ilike', 'city', $this->city])
            ->andFilterWhere(['ilike', 'street', $this->street])
            ->andFilterWhere(['ilike', 'user_id', $this->user_id]);

        return $dataProvider;
    }
}
