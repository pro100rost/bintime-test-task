<?php

namespace app\models;

use Exception;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shipping_address}}".
 *
 * @property string $id
 * @property string $zip_code
 * @property string $country
 * @property string $city
 * @property string $street
 * @property int $house_number
 * @property int $flat_number
 * @property string $user_id
 *
 * @property User $user
 */
class ShippingAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shipping_address}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'required'],
            [['id', 'user_id'], 'string'],
            [['id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],

            [['zip_code', 'country', 'city', 'street', 'house_number'], 'required'],

            [['zip_code'], 'string', 'max' => 10],
            [['zip_code'], 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'You can only use numbers.'],

            [['country'], 'string', 'max' => 2],
            [['country'], 'match', 'pattern' => '/^[A-Z]{2}$/', 'message' => 'You can only use two uppercase latin letters.'],

            [['city', 'street'], 'string', 'max' => 255],

            [['house_number', 'flat_number'], 'integer'],
            ['flat_number', 'default', 'value' => null],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zip_code' => 'Zip Code',
            'country' => 'Country',
            'city' => 'City',
            'street' => 'Street',
            'house_number' => 'House Number',
            'flat_number' => 'Flat Number',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function insert($runValidation = true, $attributeNames = null)
    {
        if (!isset($this->id)) {
            $this->id = Uuid::uuid4()->toString();
            $this->user_id = Yii::$app->request->get('id');
        }
        return parent::insert($runValidation, $attributeNames);
    }
}
