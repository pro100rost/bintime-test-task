<?php

namespace app\models;

use Ramsey\Uuid\Uuid;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $id
 * @property string $username
 * @property string $password_hash
 * @property string $first_name
 * @property string $last_name
 * @property int $genre_id
 * @property string $created_time
 * @property string $email
 *
 * @property string $password
 * @property string $confirm_password
 *
 * @property ShippingAddress[] $shippingAddresses
 */
class User extends ActiveRecord
{
    const GENRE_MALE = 1;
    const GENRE_FEMALE = 2;
    const GENRE_NO_INFORMATION = 3;

    public $password;
    public $confirm_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'username', 'password_hash', 'first_name', 'last_name', 'email', 'genre_id', 'created_time'], 'required'],
            [['id'], 'unique'],
            [['id'], 'string'],
            [['created_time'], 'safe'],

            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'match', 'pattern' => '/[A-Z]{1}[a-z]+/', 'message' => 'You can only use latin letters starting with a capital letter.'],

            ['genre_id', 'in', 'range' => [self::GENRE_MALE, self::GENRE_FEMALE, self::GENRE_NO_INFORMATION]],
            [['genre_id'], 'integer'],

            ['username', 'string', 'min' => 4],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],

            [['email'], 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'confirm_password' => 'Confirm Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'genre_id' => 'Genre',
            'created_time' => 'Created Time',
            'email' => 'Email',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getShippingAddresses()
    {
        return $this->hasMany(ShippingAddress::class, ['user_id' => 'id']);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return bool|null
     * @throws Exception
     * @throws \Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->id = Uuid::uuid4()->toString();
        $this->created_time = date('d-m-Y H:i:s');
        $this->password_hash = $this->setPassword($this->password);
        $this->setPassword($this->password);
        return $this->save();
    }

    /**
     * @return string
     */
    public function getGenre()
    {
        if ($this->genre_id === 1) {
            return 'Male';
        } elseif ($this->genre_id === 2) {
            return 'Female';
        } else {
            return 'No information';
        }
    }
}
