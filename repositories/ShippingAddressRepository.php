<?php

namespace app\repositories;

use app\models\ShippingAddress;
use yii\web\NotFoundHttpException;

class ShippingAddressRepository
{
    /**
     * Finds the ShippingAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ShippingAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findShippingAddressById($id)
    {
        if (($model = ShippingAddress::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
