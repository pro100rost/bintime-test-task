<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\ShippingAddress;
use app\models\User;
use Ramsey\Uuid\Uuid;
use Throwable;
use Yii;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TestDataController extends Controller
{
    private $maleFirstNames = [
        'Oliver',
        'Jack',
        'Harry',
        'Jacob',
        'Charlie',
        'Thomas',
        'George',
        'Oscar',
        'James',
        'William',
    ];
    private $maleLastNames = [
        'Smith',
        'Jones',
        'Williams',
        'Brown',
        'Taylor',
        'Davies',
        'Wilson',
        'Evans',
        'Thomas',
        'Roberts',
    ];
    private $femaleFirstNames = [
        'Margaret',
        'Samantha',
        'Bethany',
        'Elizabeth',
        'Joanne',
        'Megan',
        'Victoria',
        'Lauren',
        'Michelle',
        'Tracy',
    ];
    private $femaleLastNames = [
        'Wilson',
        'Smith',
        'Johnson',
        'Williams',
        'Brown',
        'Jones',
        'Miller',
        'Davis',
        'Garcia',
        'Rodriguez',
    ];
    private $emailDomens = [
        'gmail.com',
        'ukr.net',
        'mail.ru',
        'i.ua',
        'example.com',
        'yahoo.com',
        'bing.com',
        'icloud.com',
        'amazon.com',
        'github.com',
    ];

    /**
     * Inserted test data to tables
     */
    public function actionInsert()
    {
        try {
            for ($i = 0; $i < 15; $i++) {
                $genre = rand(1, 2);
                $userId = Uuid::uuid4();

                if ($genre === 1) {
                    $key = array_rand($this->maleFirstNames);
                    $username = lcfirst($this->maleFirstNames[$key]) . rand(1, 100);
                    $user = [
                        $userId,
                        $username,
                        '$2y$13$01hf9N43vy/oRB/2liNrVeb3WnEBf4pJZA9fwu3kKaNHduVGtmCAC',
                        $this->maleFirstNames[$key],
                        $this->maleLastNames[$key],
                        $genre,
                        date('d-m-Y H:i:s'),
                        $username . '@' . $this->emailDomens[$key],
                    ];
                } elseif ($genre === 2) {
                    $key = array_rand($this->femaleFirstNames);
                    $username = lcfirst($this->femaleFirstNames[$key]) . rand(1, 100);
                    $user = [
                        $userId,
                        $username,
                        '$2y$13$01hf9N43vy/oRB/2liNrVeb3WnEBf4pJZA9fwu3kKaNHduVGtmCAC',
                        $this->femaleFirstNames[$key],
                        $this->femaleLastNames[$key],
                        $genre,
                        date('d-m-Y H:i:s'),
                        $username . '@' . $this->emailDomens[$key],
                    ];
                }

                /** @var array $user */
                Yii::$app->db
                    ->createCommand()
                    ->batchInsert(
                        'user',
                        [
                            'id',
                            'username',
                            'password_hash',
                            'first_name',
                            'last_name',
                            'genre_id',
                            'created_time',
                            'email',
                        ],
                        [$user]
                    )
                    ->execute();

                for ($i = 0; $i < 8; $i++) {

                    $searchAddress = [
                        Uuid::uuid4(),
                        0 . rand(10, 99) . rand(10, 99),
                        'UA',
                        'Kyiv',
                        'Brovary avenue',
                        rand(10, 200),
                        rand(1, 100),
                        $userId,
                    ];

                    /** @var array $searchAddress */
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert(
                            'shipping_address',
                            [
                                'id',
                                'zip_code',
                                'country',
                                'city',
                                'street',
                                'house_number',
                                'flat_number',
                                'user_id',
                            ],
                            [$searchAddress]
                        )
                        ->execute();
                }
            }

            echo 'Data successfully inserted!' . PHP_EOL;
        } catch (Throwable $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * Deleted all data from tables
     */
    public function actionDelete()
    {
        User::deleteAll();
        ShippingAddress::deleteAll();

        echo 'Data successfully deleted!' . PHP_EOL;
    }
}
