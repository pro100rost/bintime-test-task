<?php

use yii\base\NotSupportedException;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%shipping_address}}`.
 */
class m190909_092151_create_shipping_address_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public function safeUp()
    {
        $this->createTable('{{%shipping_address}}', [
            'id' => $this->getDb()->getSchema()->createColumnSchemaBuilder('uuid'),
            'zip_code' => $this->string(10)->notNull(),
            'country' => $this->string(2)->notNull(),
            'city' => $this->string()->notNull(),
            'street' => $this->string()->notNull(),
            'house_number' => $this->integer()->notNull(),
            'flat_number' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_shipping_address_id', '{{%shipping_address}}', 'id');

        $this->addColumn('{{%shipping_address}}', 'user_id', 'UUID NOT NULL');
        $this->addForeignKey('fk_user_shipping_address', '{{%shipping_address}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shipping_address}}');
    }
}
