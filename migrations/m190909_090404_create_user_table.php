<?php

use yii\base\NotSupportedException;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190909_090404_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     *
     * @throws NotSupportedException
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->getDb()->getSchema()->createColumnSchemaBuilder('uuid'),
            'username' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'genre_id' => $this->smallInteger()->notNull()->defaultValue(3),
            'created_time' => $this->dateTime()->notNull(),
            'email' => $this->string()->notNull()->unique(),
        ]);

        $this->addPrimaryKey('pk_user_id', '{{%user}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
