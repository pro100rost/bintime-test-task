<?php

namespace app\bootstrap;

use app\models\NewUserForm;
use app\models\ShippingAddress;
use app\models\ShippingAddressSearch;
use app\models\User;
use app\models\UserSearch;
use app\repositories\ShippingAddressRepository;
use app\repositories\UserRepository;
use yii\base\Application;
use yii\base\BootstrapInterface;
use Yii;

class SetUp implements BootstrapInterface
{
    private $container;

    public function __construct()
    {
        $this->container = Yii::$container;
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $this->container->setSingleton(User::class);
        $this->container->setSingleton(ShippingAddress::class);
        $this->container->setSingleton(NewUserForm::class);

        $this->container->setSingleton(UserSearch::class);
        $this->container->setSingleton(ShippingAddressSearch::class);

        $this->container->setSingleton(UserRepository::class);
        $this->container->setSingleton(ShippingAddressRepository::class);
    }
}
